<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods:GET,PUT,POST,DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With,Content-Type,Accept");

require "config.php";

spl_autoload_register(function($class){
    
    if(file_exists("controladores/".$class.".php")){
        require "controladores/".$class.".php";
    }elseif(file_exists("entidades/".$class.".php")){
        require "entidades/".$class.".php";
    }else{
        if(file_exists("libs/".$class.".php")){
            require "libs/".$class.".php";
        }
    }
    
});

$controller = $_GET["controller"];
$method = $_GET["method"];

$controller = new $controller();
$controller->{$method}();

