<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animales
 *
 * @author pabhoz
 */

class Usuario {

    private $idusuario;
    private $username;
    private $uspassword;
    private $correo;
    private $url_foto;
   

    protected static $table = "usuario";

    function __construct($idusuario, $username, $uspassword, $correo, $url_foto = null) {
       $this->idusuario = $idusuario;
        $this->username = $username;
        $this->uspassword = $uspassword;
        $this->correo = $correo;
        $this->url_foto = $url_foto;
       
    }
    
    function getIdusuario() {
        return $this->idusuario;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }

    function getUsername() {
        return $this->username;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function getUspassword() {
        return $this->uspassword;
    }

    function setUspassword($uspassword) {
        $this->uspassword = $uspassword;
    }

    function getCorreo() {
        return $this->correo;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }      

    function getUrl_foto() {
        return $this->url_foto;
    }

    function setUrl_foto($url_foto) {
        $this->url_foto = $url_foto;
    }
    
    static function getTable() {
        return self::$table;
    }

    static function setTable($table) {
        self::$table = $table;
    }


    public function getAll(){
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      return $db->select("*", self::$table,"username != '".$this->getUsername()."'");
    }

       public static function getAllStatic(){
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      return $db->select("*", self::$table);
    }

      public function save(){
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $values = get_object_vars($this);
        $resultado = $db->insert(self::$table, $values);
        return $resultado;

    }

    public function login(){

        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $checkeo = $db->select("idusuario", self::$table,"correo = '".$this->getCorreo()."' AND"
                . " uspassword = '".$this->getUspassword()."'",true);
        return $checkeo;

    }

      public function cargarPerfil(){

        // $db = new DataBase("localhost", "root", "", "aguantapp");
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        // $values = get_object_vars($this);
        $resultado = $db->select("*",self::$table,"username = '".$this->getUsername()."'");

        return $resultado;

    }

    public function editarMe(){
      //$db = new DataBase("localhost", "root", "", "aguantapp");
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      $values = get_object_vars($this);
      $resultado = $db->update(self::$table,$values,"username = '".$this->getUsername()."'");

      return $resultado;
    }
  

}
