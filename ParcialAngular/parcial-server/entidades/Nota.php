<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animales
 *
 * @author pabhoz
 */

class Nota {

    private $idnota;
    private $titulo;
    private $descripcion;
    private $fecha;
    private $idlibreta;
   

    protected static $table = "nota";

    function __construct($idnota, $titulo, $descripcion, $fecha, $idlibreta) {
       $this->idnota;
       $this->titulo;
       $this->descripcion;
       $this->fecha;
       $this->idlibreta;
    }
    
    function getIdnota() {
        return $this->idnota;
    }

    function setIdnota($idnota) {
        $this->idnota = $idnota;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function getFecha() {
        return $this->fecha;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }      

    function getIdlibreta() {
        return $this->idlibreta;
    }

    function setIdlibreta($idlibreta) {
        $this->idlibreta = $idlibreta;
    }
    
    static function getTable() {
        return self::$table;
    }

    static function setTable($table) {
        self::$table = $table;
    }


    public function editarMe(){
      //$db = new DataBase("localhost", "root", "", "aguantapp");
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      $values = get_object_vars($this);
      $resultado = $db->update(self::$table,$values,"idnota = 1");

      return $resultado;
    }
  

}
