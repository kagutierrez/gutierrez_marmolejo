<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animales
 *
 * @author pabhoz
 */

class Libreta {

    private $idlibreta;
    private $nombre;    
    private $fecha;
    private $idusuario;
   

    protected static $table = "libreta";

    function __construct($idlibreta, $nombre, $fecha, $idusuario) {
       $this->idlibreta=$idlibreta;
       $this->nombre=$nombre;
       $this->fecha=$fecha;
       $this->idusuario=$idusuario;
    }
    
    function getIdlibreta() {
        return $this->idlibreta;
    }

    function setIdlibreta($idlibreta) {
        $this->idlibreta = $idlibreta;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getFecha() {
        return $this->fecha;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }      

    function getIdusuario() {
        return $this->idusuario;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }
    
    static function getTable() {
        return self::$table;
    }

    static function setTable($table) {
        self::$table = $table;
    }


    public function save(){
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $values = get_object_vars($this);
        $resultado = $db->insert(self::$table, $values);
        return $resultado;
    }
  
    public function misLibretas(){        
        $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        $checkeo = $db->select("nombre", self::$table,"idusuario = ".$this->getIdusuario(),true);
        return $checkeo;
        
    }

}
