import { Component, EventEmitter, Output, } from '@angular/core';
import { Usuario } from '../models/usuario';
import { UsuarioService } from '../services/usuario.service';
import { NgForm } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { Router } from '@angular/router';

export let __usuario = {};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  {

  @Output() emisor = new EventEmitter<string>();
  correoLogin: string ;
  usuario: Usuario;
  
  constructor(public cargarUserService:UsuarioService, private router:Router) {
  }


  iniciar(formulario: NgForm) {
    let usuario: Usuario = new Usuario(null,formulario.value.password,
      formulario.value.correo,null);
      this.mySession(usuario);
      this.cargarUserService.loginUsuario(usuario).then(response => (response >0 ? this.dirigir(response):this.showPrompt()));
  }

  showPrompt() {
    console.log("incorrectamente");
  }
  dirigir(response){
    console.log("id complido "+response);
   __usuario = response;
   console.log("usuario"+__usuario);
  this.router.navigate(['/integrador'])
  //this.router.navigate(['/nota']); 
  }
  public mySession(usuario){
    this.cargarUserService.crearSesion(usuario).then(response => console.log(response));
 }

 emit(){
  this.emisor.emit(this.correoLogin);
  console.log(this.correoLogin);
}
 
}
