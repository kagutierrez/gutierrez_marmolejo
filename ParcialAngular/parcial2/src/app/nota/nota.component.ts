import { Component, OnInit , Output, EventEmitter, SimpleChanges } from '@angular/core';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-nota',
  templateUrl: './nota.component.html',
  styleUrls: ['./nota.component.css']
})
export class NotaComponent implements OnInit {

  constructor() { }

  @Output() emisor = new EventEmitter<string>();
  tituloNota: string ;
  cuerpoNota: string;
  tituloNotaEditar: string;

  ngOnInit() {
  }


  add(){
  /*  this.nota.push(
      {
        name:this.nombre,
        thumb:"bar",
        time:560
      }
    );*/
  }

  emit(){
    this.emisor.emit(this.tituloNota);
    this.emisor.emit(this.cuerpoNota);
    console.log(this.tituloNota);
    console.log("este es el cuerpo: "+this.cuerpoNota)
    
    
  }

  interceptar($event){
    this.tituloNotaEditar = $event;
  }
}
