import { Injectable } from '@angular/core';
import { Libreta } from '../models/libreta';
import { Nota } from '../models/nota';
import { config } from './config';
import {Http, Headers} from '@angular/http';
import 'rxjs';

@Injectable()
export class LibretaService {

  libretas:Libreta[]=[];
  notas:Nota[]=[];



  constructor(private http: Http) { }



  crearLibreta(titulo:String,__usuario:object) {
    let params = "&nombre="+titulo+"&id="+__usuario;
    return this.http.get(config.server+"?controller=Libreta_Controller&method=insertar"+params).toPromise()
    .then( data => data.json(), err => err);          
  }

  verLibretas(__usuario:object){
    let params = "&id="+__usuario;
    return this.http.get(config.server+"?controller=Libreta_Controller&method=misLibretas"+params).toPromise()
    .then( data => data.json(), err => err);    
  }

}