import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs';

import { config } from './config';
import { Usuario } from '../models/usuario';


@Injectable()
export class UsuarioService {

  usuario: Usuario;

  constructor(private http: Http) {}

  registrarUsuario(usuario:Usuario) {
    let params = "&nombre="+usuario.username+"&contraseña="+usuario.password+"&email="+usuario.correo;
    return this.http.get(config.server+"?controller=Usuarios_Controller&method=post"+params).toPromise()
    .then( data => {data.toString(); console.log("EL nombre del usuario: "+usuario.username)}, err => console.log(err));    
   // console.log("EL nombre del usuario: "+usuario.username);
   
  }

  loginUsuario(usuario:Usuario) {
    let params = "&email="+usuario.correo+"&contraseña="+usuario.password;
    return this.http.get(config.server+"?controller=Usuarios_Controller&method=login"+params).toPromise()
    .then( data =>data.json()[0]['idusuario'], err => err);          
    
  }

  crearSesion(usuario:Usuario){
    let params = "&email="+usuario.correo+"&contraseña="+usuario.password;
        //  console.log(params);
    var url = config.server+"?controller=Usuarios_Controller&method=crearSession"+params;
    return this.http.get(url)
                .toPromise()
                .then( data => data.toString(), err => err);
  }

}
