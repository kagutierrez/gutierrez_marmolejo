import { Component} from '@angular/core';
import { Usuario } from '../models/usuario';
import { UsuarioService } from '../services/usuario.service';
import { NgForm } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})

export class RegistroComponent  {
 
  usuario: Usuario;
  
  constructor(public cargarUserService:UsuarioService) {
  }


  addPost(formulario: NgForm) {
    let usuario: Usuario = new Usuario(formulario.value.username,formulario.value.password,
      formulario.value.correo,formulario.value.foto);
      this.cargarUserService.registrarUsuario(usuario).then(response => { this.showPrompt();          
  });
    formulario.reset;  
  }


  showPrompt() {
    console.log("El usuario se inserto correctamente");
  }
   
 
}


