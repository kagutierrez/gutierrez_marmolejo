import { Nota } from "./nota";

export class Libreta {
    titulo:string;
    notas : Nota[]=[];

    constructor(titulo:string, notas?:Nota[]){
        
        this.titulo= titulo;
        this.notas=notas;
        
    }

    addNotas(nota: Nota){
        this.notas.push(nota);
    }
}
