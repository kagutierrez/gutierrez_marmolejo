export class Nota {

    titulo:string;
    cuerpo:string;
    fecha:string;
    id:string;
    constructor(titulo:string, cuerpo:string,fecha:string, id:string){
        
        this.titulo= titulo;
        this.cuerpo=cuerpo;
        this.fecha=fecha;
        this.id=id;
        
    }

}
