
export class Usuario{

    username: string;
    password: string;
    correo:string;
    foto: string;

    constructor(username: string, password: string,correo:string, foto: string) {
        this.username = username;
        this.password = password;
        this.correo=correo;
        this.foto = foto;
    }

}