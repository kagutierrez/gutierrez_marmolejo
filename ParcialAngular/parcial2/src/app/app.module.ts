import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';
import { UsuarioService } from './services/usuario.service';
import { LibretaService } from './services/libreta.service';
import { Http, Headers, HttpModule } from '@angular/http';
import { LoginComponent } from './login/login.component';
import { NotaComponent } from './nota/nota.component';
import { RouterModule, Routes } from '@angular/router';
import { NotfoundComponent } from './notfound/notfound.component';
import { IntegradorComponent } from './integrador/integrador.component';
import { LibretasComponent } from './libretas/libretas.component';
import { UsuarioComponent } from './usuario/usuario.component';



const appRoutes: Routes =[
  
    {
      path: 'login',
      component: LoginComponent
    },{
      path: 'registro',
      component: RegistroComponent,
      pathMatch: 'full'
    },{
      path: 'login/registro',
      component: RegistroComponent,
      pathMatch: 'full'
    },{
      path: 'integrador',
      component: IntegradorComponent
    },{
      path: 'login/integrador',
      component: IntegradorComponent,
      pathMatch:'full'
    }
    ,{
      path: '',
      redirectTo: '/login',
      pathMatch:'full'
    },
    { path: '**', component: NotfoundComponent}
  ];


@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    LoginComponent,
    NotaComponent,
    NotfoundComponent,
    IntegradorComponent,
    LibretasComponent,
    UsuarioComponent
 
  ],
  imports: [
    BrowserModule,
    FormsModule,    
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } 
    )
    
    
  ],
  providers: [
    UsuarioService,
    LibretaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
