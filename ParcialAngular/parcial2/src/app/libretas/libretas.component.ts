import { Component, OnInit, Input ,Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NgModel } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { LibretaService } from '../services/libreta.service';
import { __usuario } from '../login/login.component';

@Component({
  selector: 'app-libretas',
  templateUrl: './libretas.component.html',
  styleUrls: ['./libretas.component.css']
})
export class LibretasComponent implements OnInit {

  today: number = Date.now();
  
    /*@Input() tituloNota: string;
    @Input() cuerpoNota: string;*/
    
    titulo:string = "Nota sin titulo";
    nota: any = [];
    Fecha : string ="26/10/17"
    
    constructor(public libretaService:LibretaService) { }
    
  
    //@Output() emisor = new EventEmitter<string>();

  ngOnInit() {
    this.libretaService.verLibretas(__usuario).then(response => (response ? this.mostrarLibretas(response):console.log("error")));
  }

  add(formulario: NgForm){
    this.nota.push({
      titulo: formulario.value.nombreLibreta,
      fecha: Date()
    });
    this.libretaService.crearLibreta(formulario.value.nombreLibreta,__usuario);//.then(response => (response ? this.dirigir():this.showPrompt()));

  }

  mostrarLibretas(response){ 
    for(let i=0;i<response.length;i++){
      this.nota.push({
        titulo: response[i]['nombre'],
        fecha: Date()
      });
    }
  }


  borrarLibreta(response){
   
    for (var index = 0; index < response.length; index++) {
      if (this.nota[index].titulo == this.nota.titulo) {
        this.nota.splice(index);
        
      }
      
    }

  }

  

  //tituloNotaEditar: string=this.tituloNota;

  /*emit(){
  console.log("entro aqui papu y envio"+this.tituloNota);
  this.emisor.emit(this.tituloNotaEditar);
  }*/
  
}
