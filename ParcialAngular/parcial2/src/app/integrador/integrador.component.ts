import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-integrador',
  templateUrl: './integrador.component.html',
  styleUrls: ['./integrador.component.css']
})
export class IntegradorComponent implements OnInit {
  temporal: string;
  constructor() { }

  ngOnInit() {
  }

  interceptar($event){
    this.temporal = $event;
  }
}
